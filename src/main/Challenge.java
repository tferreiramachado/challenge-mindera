package main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Challenge {
	static WebDriver driver;

public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		
		driver = new ChromeDriver();
        driver.get("http://localhost:3000");
        
        dragAndDropToUpOfItem("Item 4", "Item 5");
        dragAndDropToUpOfItem("Item 3", "Item 4");
        dragAndDropToUpOfItem("Item 2", "Item 3");
        dragAndDropToUpOfItem("Item 1", "Item 2");
        dragAndDropToUpOfItem("Item 0", "Item 1");
        
        driver.quit();
	}

	public static void dragAndDropToUpOfItem(String from, String to) {
		Actions action = new Actions(driver);
	    
	    WebElement elementFrom = driver.findElement(By.xpath("//li[contains(text(), '"+ from +"')]"));
	    WebElement elementTo = driver.findElement(By.xpath("//li[contains(text(), '"+ to +"')]"));
	    
	    action.dragAndDropBy(elementFrom, 0, elementTo.getLocation().getY() - elementFrom.getLocation().getY() - 30).build().perform();
	}
}