package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

class TestApp {
	
	private static WebDriver driver;
	
	@BeforeEach
	void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:/Users/ago_f/eclipse-workspace/qa-mindera/chromedriver.exe");
		
		driver = new ChromeDriver();
        driver.get("http://localhost:3000");
	}

	@AfterEach
	void tearDown() throws Exception {
		driver.quit();
	}
	
	@Test
	void testNumberOfItems() {
		List<WebElement> arrayItems = driver.findElements(By.cssSelector("#app li"));
		assertEquals(6, arrayItems.size(), "The number of elements is correct.");
	}

	@Test
	void testContentOfItems() {
		List<WebElement> arrayItems = driver.findElements(By.cssSelector("#app li"));
		List<String> arrayTextItems = new ArrayList<String>();
		
		for (WebElement element : arrayItems) {
			arrayTextItems.add(element.getText());
		}
		
		assertTrue(arrayTextItems.contains("Item 0"), "Item 0 is on the list");
		assertTrue(arrayTextItems.contains("Item 1"), "Item 1 is on the list");
		assertTrue(arrayTextItems.contains("Item 2"), "Item 2 is on the list");
		assertTrue(arrayTextItems.contains("Item 3"), "Item 3 is on the list");
		assertTrue(arrayTextItems.contains("Item 4"), "Item 4 is on the list");
		assertTrue(arrayTextItems.contains("Item 5"), "Item 5 is on the list");
		assertFalse(arrayTextItems.contains("Item 6"), "Item 6 is not on the list");
	}
	
	@Test
	void testItemsAreDraggable() {
		Actions action = new Actions(driver);
		
		//Drag Item 0 and drop on the top of the list
		WebElement element = driver.findElement(By.xpath("//li[contains(text(), 'Item 0')]"));
	    action.dragAndDropBy(element, 0, -500).build().perform();
	    
	    List<WebElement> arrayItems = driver.findElements(By.cssSelector("#app li"));
	    assertEquals("Item 0", arrayItems.get(0).getText(), "Item 0 is on the top of the list");
	    
	  //Drag Item 0 and drop on the bottom of the list
	    element = driver.findElement(By.xpath("//li[contains(text(), 'Item 0')]"));
	    action.dragAndDropBy(element, 0, 500).build().perform();
	    
	    arrayItems = driver.findElements(By.cssSelector("#app li"));
	    assertEquals("Item 0", arrayItems.get(5).getText(), "Item 0 is on the bottom of the list");
	}
}
