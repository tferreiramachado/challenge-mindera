﻿## Setup

1. Clone this repository.
2. Download and install Java Runtime Environment (JRE).
3. Download and install Eclipse IDE for Java Developers. Link: http://www.eclipse.org/downloads/packages/release/2018-09/r/eclipse-ide-java-developers
4. Import the project that was cloned.

---

## Run Challenge script

1. In Eclipse, open the file Challenge.java on main package.
2. Press "Run" button.

Note: The browser will quit after ending the sort. To see the result, comment or remove the line that contains "driver.quit();".

---

## Run Tests

1. In Eclipse, open the file TestApp.java on test package.
2. Press "Run" button.

---

## Bug Report

1. The file "Bug Report/bugsApp.txt" contains the details of a bug found in application.